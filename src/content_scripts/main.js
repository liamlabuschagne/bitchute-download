browser.runtime.onMessage.addListener(() => {
  const iframe = document.querySelectorAll("iframe");
  let videos = [];
  videos.push(...document.querySelectorAll("video source"));
  if (iframe) {
    for (let i = 0; i < iframe.length; i++) {
      if (iframe[i].contentDocument)
        videos.push(...iframe[i].contentDocument.querySelectorAll("video"));
    }
  }
  console.log(videos);
  let videoSources = [];
  videos.forEach((video) => {
    videoSources.push(video.src);
  });

  browser.runtime.sendMessage(videoSources);
});
