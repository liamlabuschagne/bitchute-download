browser.tabs.executeScript({ file: "/content_scripts/main.js" }).then(() => {
  browser.tabs.query({ active: true, currentWindow: true }).then((tabs) => {
    browser.tabs.sendMessage(tabs[0].id, {});
  });

  browser.runtime.onMessage.addListener((request) => {
    // Clear the popup
    document.querySelector("#links").innerHTML = "";

    request.forEach((videoSource) => {
      // Create a download a tag
      var a = document.createElement("a");
      a.href = videoSource;
      a.textContent = videoSource;
      a.style.display = "block";
      a.style.margin = "10px";
      // Add the link to the popup
      document.querySelector("#links").appendChild(a);
    });
  });
});
